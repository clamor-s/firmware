# SPDX-License-Identifier: GPL-2.0+
# (C) Copyright 2024 The Android Open Source Project
# NVRAM config file for the Azurewave NH615 BCM4329 WiFi/BT module
# Found in the ASUS Transformer TF300T/TG/TL
aa2g=3
ag0=0x0
ag1=0x0
boardflags=0x00001200
boardrev=0x11
boardtype=0x5df
bxa2g=0
cckdigfilttype=1
cctl=0x0
devid=0x432f
hwhdr=0x05ffff031030031003100000
mcs2gpo0=0x8888
mcs2gpo1=0x8888
nocrc=1
ofdmpo=0x44444444
otpimagesize=182
pa0itssit=62
pa0maxpwr=74
RAW1=80 32 fe 21 02 0c 00 22 2a 01 01 00 00 c5 0 e6 00 00 00 00 00 40 00 00 ff ff 80 00 00 00 00 00 00 00 00 00 00 c8 00 00 00 00 00 00 00 00 00 00 00 00 00 ff 20 04 D0 2 29 43 21 02 0c 00 22 04 00 20 00 5A
rssisav2g=0x3,0x3,0x3
rssismc2g=0xb,0xb,0xa
rssismf2g=0xa,0xa,0xa
rxpo2g=0
sromrev=3
vendid=0x14e4
xtalfreq=37400
